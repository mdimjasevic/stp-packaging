#!/bin/bash

set -e

group=$(id -g)
username=$(id -un)

cat >>~/.bashrc <<"EOF"
# DEBEMAIL="marko@dimjasevic.net"
# DEBFULLNAME="Marko Dimjašević"
# export DEBEMAIL DEBFULLNAME
alias ll='ls -l'
alias la='ls -al'
EOF

# Set up quilt
cat >>~/.quiltrc <<"EOF"
d=. ; while [ ! -d $d/debian -a `readlink -e $d` != / ]; do d=$d/..; done
if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then
    # if in Debian packaging tree with unset $QUILT_PATCHES
    QUILT_PATCHES="debian/patches"
    QUILT_PATCH_OPTS="--reject-format=unified"
    QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto"
    QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
    QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33:diff_ctx=35:diff_cctx=33"
    if ! [ -d $d/debian/patches ]; then mkdir $d/debian/patches; fi
fi
EOF

source ~/.bashrc

# Install Debian developer tools
sudo apt-get install --yes build-essential debhelper dh-make fakeroot gnupg lintian patch patchutils quilt pbuilder

# Optional Debian developer packages
# sudo apt-get install --yes autoconf automake autotools-dev devscripts xutils-dev


# Installs dependencies
dependencies="cmake zlib1g-dev flex bison minisat python libboost-program-options-dev libboost-system-dev llvm-3.5-tools libgtest-dev python"
sudo apt-get install --yes $dependencies

# Misc tools needed
sudo apt-get install --yes wget

# Package name and version
package=stp
upstream_version=2.1.1
version="${upstream_version}+dfsg"
package_version=${package}-${version}
# package revision
package_revision="1"
package_dir=${package_version}
package_dir_absolute=~/${package_version}
archive_ext=tar.gz
archive_name="${package_version}.${archive_ext}"

# Upstream values
upstream_name=stp
upstream_archive=${upstream_name}-${upstream_version}.tar.gz
upstream_dir=${upstream_name}-${upstream_version}

cd
wget https://github.com/stp/stp/archive/${upstream_version}.tar.gz -O ${upstream_archive}
tar xf ${upstream_archive}
rm -rf ${upstream_dir}/papers
mv ${upstream_dir} ${package_dir}

# Start packaging
export DEBEMAIL="marko@dimjasevic.net"
export DEBFULLNAME="Marko Dimjašević"
extra_exclude="--exclude=.gitignore --exclude=.gitmodules"
# Important: don't tar by providing an absolute path to the target!
tar czf ${archive_name} --exclude-vcs ${extra_exclude} ${package_dir}

# OutputCheck is needed for testing
# OutputCheck git commit used: e0f533d3c5
outputcheck_archive="${package}_${version}.orig-outputcheck.tar.gz"
outputcheck_archive_dir=${package_dir_absolute}/..
cp /vagrant/${outputcheck_archive} ${outputcheck_archive_dir}

cd ${package_dir_absolute}
dh_make --multi --yes --file ../${archive_name}

# Update debian/control
cp /vagrant/debian/control ${package_dir_absolute}/debian/

# Copy .install files for binary packages
cp /vagrant/debian/*.install ${package_dir_absolute}/debian/

# Update copyright files
rm ${package_dir_absolute}/debian/copyright
cp /vagrant/debian/*.copyright ${package_dir_absolute}/debian/

# Update debian/changelog
cp /vagrant/debian/changelog ${package_dir_absolute}/debian/

# Update debian/rules
cp /vagrant/debian/rules ${package_dir_absolute}/debian/

# Note a removal of research papers in PDF from the package in
# debian/README.Debian
cp /vagrant/debian/README.Debian ${package_dir_absolute}/debian/

# Remove README.source as we don't need it
rm ${package_dir_absolute}/debian/README.source

# Nothing to do with debian/compat as its content is already set to "9"

# docs
cp ${package_dir_absolute}/debian/docs ${package_dir_absolute}/debian/stp.docs
mv ${package_dir_absolute}/debian/docs ${package_dir_absolute}/debian/python-stp.docs

# Man pages
rm --force ${package_dir_absolute}/debian/manpage.*
manpage_section=1
manpage=${package}.${manpage_section}
cp /vagrant/debian/${manpage} ${package_dir_absolute}/debian/
echo "debian/${manpage}" > ${package_dir_absolute}/debian/${package}.manpages

# Copy menu
cp /vagrant/debian/menu ${package_dir_absolute}/debian/

# Copy watch (used by uscan(1))
cp /vagrant/debian/watch ${package_dir_absolute}/debian/

# Remove all example files from debian/
rm --force ${package_dir_absolute}/debian/*.{ex,EX}


# Apply the patch for STP executables
cd ${package_dir_absolute}
mkdir -p debian/patches

# quilt new fix-executables.patch
# quilt add LICENSE
# quilt add CMakeLists.txt
# quilt add tools/CMakeLists.txt
# quilt add tools/stp/CMakeLists.txt
# quilt add tools/stp_simple/*
# cp /vagrant/stp-with-a-patch/LICENSE .
# cp /vagrant/stp-with-a-patch/CMakeLists.txt .
# cp /vagrant/stp-with-a-patch/tools/CMakeLists.txt tools/
# cp /vagrant/stp-with-a-patch/tools/stp/CMakeLists.txt tools/stp/
# rm -rf tools/stp_simple/
# quilt refresh
# quilt header -r "Executables reorganized: removed stp_simple, removed a symlink in usr/bin/"

patch_p1=fix-executables.patch
quilt import -P ${patch_p1} /vagrant/${patch_p1}
quilt push -a

# Perform a complete build
cd ${package_dir_absolute}
# DEB_BUILD_OPTIONS='parallel=8' fakeroot debian/rules binary
dpkg-buildpackage -us -uc -j8


# pbuilder

sudo chgrp $group /var/cache/pbuilder/result
sudo chmod g+w /var/cache/pbuilder/result

hook_dir=/var/cache/pbuilder/hooks
sudo mkdir ${hook_dir}
sudo chgrp $group ${hook_dir}
sudo chmod g+w ${hook_dir}

cat >>~/.pbuilderrc <<EOF
AUTO_DEBSIGN=${AUTO_DEBSIGN:-no}
HOOKDIR=${hook_dir}
EOF
sudo cp ~/.pbuilderrc /root/

# Initialize the local pbuilder chroot system
sudo pbuilder create

cd
sudo pbuilder --update
sudo pbuilder --build ${package}_${version}-${package_revision}.dsc


# Use sbuild for cross-compiling
# sudo sbuild-adduser $username
# mkdir -p ~/.gnupg
# sbuild-update --keygen
