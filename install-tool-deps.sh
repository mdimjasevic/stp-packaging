#!/bin/bash

# Installs dependencies for experimental KLEE based on LLVM 3.4
dependencies="cmake zlib1g-dev flex bison minisat python libboost-program-options-dev libboost-system-dev llvm-3.6-tools libgtest-dev python"
sudo apt-get install --yes $dependencies


