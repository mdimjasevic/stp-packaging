FROM sid:amd64
MAINTAINER Marko Dimjašević <marko@dimjasevic.net>

ENV DEBIAN_FRONTEND noninteractive

ENV home_dir /home/docker

# STP packaging values
ENV package stp
ENV org $package
ENV upstream_version 2.1.2
ENV version="${upstream_version}+dfsg"
ENV package_version $package-$version
# package revision
ENV package_revision 1
ENV package_dir $package_version
ENV package_dir_absolute "${home_dir}/${package_version}"
ENV archive_ext "tar.gz"
ENV archive_name "${package_version}.${archive_ext}"
# Upstream values
ENV upstream_name stp
ENV upstream_archive ${upstream_name}-${upstream_version}.${archive_ext}
ENV upstream_dir ${upstream_name}-${upstream_version}

# Install needed basics not present in the Docker image
RUN apt-get update
RUN apt-get install --yes apt-utils
RUN apt-get install --yes bash wget sudo

# Create a sudo user "docker"
RUN adduser --disabled-password docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Set up basics for packaging
ADD set-up-basics.sh /tmp/
RUN su --command /tmp/set-up-basics.sh docker

# Install STP dependencies
ADD install-tool-deps.sh /tmp/
RUN su --command /tmp/install-tool-deps.sh docker

# Current dir and user
WORKDIR ${home_dir}
USER docker

# Get STP and unpack it
RUN wget https://github.com/${org}/${package}/archive/${upstream_version}.tar.gz -O ${upstream_archive}
RUN tar xf ${home_dir}/${upstream_archive}
RUN rm -rf ${upstream_dir}/papers
RUN mv ${upstream_dir} ${package_dir}

# Start packaging STP
ENV DEBEMAIL "marko@dimjasevic.net"
ENV DEBFULLNAME "Marko Dimjašević"
ENV extra_exclude "--exclude=.gitignore --exclude=.gitmodules"
# Important: don't tar by providing an absolute path to the target!
RUN tar czf ${archive_name} --exclude-vcs ${extra_exclude} ${package_dir}

# OutputCheck is needed for testing
# OutputCheck git commit used: e0f533d3c5
ENV outputcheck_archive "${package}_${version}.orig-outputcheck.tar.gz"
ENV outputcheck_archive_dir ${home_dir}/
COPY ${outputcheck_archive} ${outputcheck_archive_dir}

WORKDIR ${package_dir_absolute}
RUN dh_make --yes --single --file ../${archive_name}

# Update debian/control
COPY debian/control ${package_dir_absolute}/debian/

# Copy .install files for binary packages
COPY debian/*.install ${package_dir_absolute}/debian/

# Update the copyright file
RUN rm ${package_dir_absolute}/debian/copyright
COPY debian/copyright ${package_dir_absolute}/debian/

# Update debian/changelog
COPY debian/changelog ${package_dir_absolute}/debian/

# Update debian/rules
COPY debian/rules ${package_dir_absolute}/debian/

# Note a removal of research papers in PDF from the package in
# debian/README.Debian
COPY debian/README.Debian ${package_dir_absolute}/debian/

# Remove README.source as we don't need it
RUN rm ${package_dir_absolute}/debian/README.source

# Nothing to do with debian/compat as its content is already set to "9"

# docs
ADD debian/stp.docs ${package_dir_absolute}/debian/
ADD debian/python-stp.docs ${package_dir_absolute}/debian/

# Man pages
RUN rm --force ${package_dir_absolute}/debian/manpage.*
ENV manpage_section 1
ENV manpage "${package}.${manpage_section}"
COPY debian/${manpage} ${package_dir_absolute}/debian/
RUN echo "debian/${manpage}" > ${package_dir_absolute}/debian/${package}.manpages

# Copy menu
COPY debian/menu ${package_dir_absolute}/debian/

# Copy watch (used by uscan(1))
COPY debian/watch ${package_dir_absolute}/debian/

# Copy the Debian source format file
COPY debian/source/format ${package_dir_absolute}/debian/source/

# Copy the Lintian overrides file
COPY debian/source/lintian-overrides ${package_dir_absolute}/debian/source/

# Remove all example files from debian/
RUN rm --force ${package_dir_absolute}/debian/*.ex
RUN rm --force ${package_dir_absolute}/debian/*.EX

# Apply the patch for STP executables
WORKDIR ${package_dir_absolute}
RUN mkdir -p debian/patches

ENV patch_p1 fix-executables.patch
COPY ${patch_p1} /tmp/
RUN quilt import -P ${patch_p1} /tmp/${patch_p1}
RUN quilt push -a

# Change file permissions of copied files
USER root
RUN chown docker:docker ${home_dir}/${klee_uclibc_archive}
RUN chown --recursive docker:docker ${package_dir_absolute}
USER docker

WORKDIR ${package_dir_absolute}
RUN dpkg-buildpackage -us -uc -j8
